<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmailJob;
use App\Mail\TrainingCreatedMail;
use App\Models\Training;
use File;
use Illuminate\Http\Request;
use Mail;
use Session;
use Storage;

class TrainingController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->carian != null){
            $trainings = Training::where('title','like','%'.$request->carian.'%')->paginate(2);
        } else {
            $trainings = Training::with('user')->paginate(2);
        }
        return view('trainings.index', compact('trainings'));
    }

    public function create()
    {
        return view('trainings.create');
    }

    public function store(Request $request)
    {
        //POPO | Plain Old PHP Object
        // $training = new Training();
        // $training->title = $request->title;
        // $training->description = $request->description;
        // $training->user_id = auth()->user()->id;
        // $training->save();

        $user = auth()->user();
        $training = $user->trainings()->create($request->only('title','description'));

        if ($request->hasFile('attachment')) {
            $filename = $training->id.'-'.date('Y-m-d').'.'.$request->attachment->getCLientOriginalExtension();
            $training->update(['attachment'=>$filename]);
            Storage::disk('public')->put($filename,File::get($request->attachment));
        }

        // Mail::send('emails.training-created', [
        //         'title'=>$request->title,
        //         'description'=>$request->description
        //     ], function ($message) {
        //     $message->from(env('MAIL_FROM_ADDRESS'));
        //     $message->sender(env('MAIL_FROM_ADDRESS'), 'Masibai');
        //     $message->to('AhYie@hellsystem.com.my', 'Dragon of Pasir Mas');
        //     $message->subject('Resignation Letter');
        // });

        // Mail::to('AhYieCekci@hellsystem.com')->send(new TrainingCreatedMail($training));

        dispatch(new SendEmailJob($training));

        return redirect(Route('training:index'))->with([
            'alert-type'=>'alert-primary',
            'alert'=>'Your training has been saved.'
        ]);
    }

    public function show(Training $training)
    {
        $this->authorize('view',$training);
        return view('trainings.show',compact('training'));
    }

    public function edit(Training $training)
    {
        return view('trainings.edit',compact('training'));
    }

    public function update(Training $training, Request $request)
    {
        // $training->title=$request->title;
        // $training->description=$request->description;
        // $training->save();

        $training->update($request->only('title','description'));

        return redirect()->route('training:index')->with([
            'alert-type'=>'alert-success',
            'alert'=>'Your training has been updated.'
            ]);
    }

    public function delete(Training $training)
    {
        if ($training->attachment!=null) {
            Storage::disk('public')->delete($training->attachment);
        }
        $training->delete();
        return redirect()->route('training:index')->with([
            'alert-type'=>'alert-danger',
            'alert'=>'Your training has been deleted.'
            ]);
    }
}
