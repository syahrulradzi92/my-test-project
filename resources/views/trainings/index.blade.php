@extends('admin.layouts.main')

@section('content')
    <div class="container-fluid">
        <h1 class="mt-4">Training Module</h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Training List</li>
        </ol>
        {{-- @if (session()->has('alert'))
            <div class="alert {{session()->get('alert-type')}}" role="alert">
                {{session()->get('alert')}}
            </div>
        @endif --}}
        <div class="row">
            <div class="col-md-12"> 
                @if (Session::has('alert'))
                    <div class="alert {{Session::get('alert-type')}}" role="alert">
                        {{Session::get('alert')}}
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        Training Index
                        <div class="float-right">
                            <form action="{{Route('training:index')}}" method="GET">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="carian" value="{{request()->get('carian')}}">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit">Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Bil.</th>
                                    {{-- <th>ID</th> --}}
                                    <th>Title</th>
                                    <th>Creator</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($trainings as $key=>$training)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        {{-- <td>{{$training->id}}</td> --}} 
                                        @can('view', $training)
                                            <td><a href="{{Route('training:show',['training'=>$training->id])}}">{{$training->title}}</a></td>
                                        @endcan
                                        @cannot('view', $training)
                                            <td>{{$training->title}}</td> 
                                        @endcannot
                                        <td>{{$training->user->name}}</td>
                                        <td>{{$training->created_at}}</td>
                                        <td>
                                            <div class="btn-group">
                                                @can('update', $training)
                                                    <button class="btn btn-info"><a style="text-decoration:none;color: white" href="{{Route('training:edit',['training'=>$training->id])}}">Edit</a></button>
                                                @endcan
                                                @can('update', $training)
                                                    <button class="btn btn-danger"><a onclick="return confirm('Yakin delete ni?')" style="text-decoration:none;color: white" href="{{Route('training:delete',$training)}}">Delete</a></button>
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{
                            $trainings->appends([
                                'carian'=>request()->get('carian')
                            ])->links()
                        }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection