@extends('admin.layouts.main')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Training Create Form</div>
                    <div class="card-body">
                        <form action="" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" id="description" cols="20" rows="5" class="form-control"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Attachment</label>
                                <input type="file" name="attachment" class="form-control">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Store My Training</button>
                                <button class="btn btn-danger"><a style="text-decoration:none;color: white" href="{{url()->previous()}}">Back</a></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection