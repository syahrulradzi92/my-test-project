@extends('admin.layouts.main')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Show Training</div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control" value="{{$training->title}}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" id="description" cols="20" rows="5" class="form-control" readonly>{{$training->description}}</textarea>
                        </div>
                        <div>
                            <button class="btn btn-danger"><a style="text-decoration:none;color: white" href="{{url()->previous()}}">Back</a></button>
                        </div>
                        <div class="form-group">
                            <a href="{{$training->attachment_url}}">View Attachment</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection